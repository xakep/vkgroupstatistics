﻿namespace VKGroupStats
{
    public enum Platform
    {
        Mobile = 1,
        Iphone = 2,
        Ipad = 3,
        Android = 4,
        Wphone = 5,
        Windows = 6,
        Web = 7
    }

    public static class PlatformExtensions
    {
        public static string ToFriendlyString(this Platform platform)
        {
            switch (platform)
            {
                case Platform.Mobile:
                    return "Mobile website version or unidentified mobile app";
                case Platform.Iphone:
                    return "Official app for iPhone";
                case Platform.Ipad:
                    return "Official app for iPad";
                case Platform.Android:
                    return "Official app for Android";
                case Platform.Wphone:
                    return "Official app for Windows Phone";
                case Platform.Windows:
                    return "windows Official app for Windows 8";
                case Platform.Web:
                    return "Full website version or unidentified apps";
                default:
                    return "n/a";
            }
        }
    }
}
