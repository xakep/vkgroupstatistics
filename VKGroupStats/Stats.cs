﻿namespace VKGroupStats
{
    using Microsoft.Extensions.Configuration;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;

    public class Stats
    {
        public int Count { get; private set; }

        public int FemalesCount { get; private set; }

        public float FemalesPercent { get; private set; }

        public int MalesCount { get; private set; }

        public float MalesPercent { get; private set; }

        public string MaleMostName { get; private set; }

        public int MaleMostNameCount { get; private set; }

        public string FemaleMostName { get; private set; }

        public int FemaleMostNameCount { get; private set; }

        public int HaveAge { get; private set; }

        public float HaveAgePer { get; private set; }

        public int MalesWithAge { get; private set; }

        public float MalesWithAgePer { get; private set; }

        public int FemalesWithAge { get; private set; }

        public float FemalesWithAgePer { get; private set; }

        public float AvgAge { get; private set; }

        public float MalesAvgAge { get; private set; }

        public float FemalesAvgAge { get; private set; }

        public int HaveCountry { get; private set; }

        public float HaveCountryPer { get; private set; }

        public int HaveCity { get; private set; }

        public float HaveCityPer { get; private set; }

        public int UniqCountries { get; private set; }

        public int UniqCities { get; private set; }

        public Dictionary<int, int> Ages { get; private set; }

        public Dictionary<string, Dictionary<string, int>> GEO { get; private set; }

        public Dictionary<Platform, int> Platforms { get; private set; }

        public IEnumerable<int> CurrentUserIds { get; private set; }

        public IEnumerable<int> PreviousUserIds { get; private set; }

        public Stats(IEnumerable<User> users, IEnumerable<int> previousIds = null)
        {
            this.Count = users.Count();

            this.MalesCount = users.Where(u => u.Sex == Sex.Male).Count();
            this.FemalesCount = users.Where(u => u.Sex == Sex.Female).Count();
            this.MalesPercent = ((float)(this.MalesCount * 100) / this.Count);
            this.FemalesPercent = ((float)(this.FemalesCount * 100) / this.Count);

            var maleNames = users.Where(u => u.Sex == Sex.Male && !string.IsNullOrEmpty(u.First_name))
                .GroupBy(u => u.First_name).OrderByDescending(g => g.Count());
            var femaleNames = users.Where(u => u.Sex == Sex.Female && !string.IsNullOrEmpty(u.First_name))
                .GroupBy(u => u.First_name).OrderByDescending(g => g.Count());

            this.MaleMostName = maleNames.First().Key;
            this.MaleMostNameCount = maleNames.First().Count();
            this.FemaleMostName = femaleNames.First().Key;
            this.FemaleMostNameCount = femaleNames.First().Count();

            var usersWithDobs = users.Where(u => !string.IsNullOrEmpty(u.Bdate) && u.Bdate.Split('.').Count() == 3);

            this.HaveAge = usersWithDobs.Count();
            this.HaveAgePer = ((float)(this.HaveAge * 100) / this.Count);

            this.MalesWithAge = usersWithDobs.Where(u => u.Sex == Sex.Male).Count();
            this.MalesWithAgePer = ((float)(this.MalesWithAge * 100) / this.MalesCount);
            this.FemalesWithAge = usersWithDobs.Where(u => u.Sex == Sex.Female).Count();
            this.FemalesWithAgePer = ((float)(this.FemalesWithAge * 100) / this.FemalesCount);

            this.Ages = this.ParseDOBs(usersWithDobs);

            this.AvgAge = (float)usersWithDobs.Average(u => u.Age);
            this.MalesAvgAge = (float)usersWithDobs.Where(u => u.Sex == Sex.Male).Average(u => u.Age);
            this.FemalesAvgAge = (float)usersWithDobs.Where(u => u.Sex == Sex.Female).Average(u => u.Age);

            var usersWithCountry = users.Where(u => u.Country != null);
            this.HaveCountry = usersWithCountry.Count();
            this.HaveCountryPer = ((float)(this.HaveCountry * 100) / this.Count);

            var usersWithCity = users.Where(u => u.City != null);
            this.HaveCity = usersWithCity.Count();
            this.HaveCityPer = ((float)(this.HaveCity * 100) / this.Count);

            this.UniqCountries = usersWithCountry.GroupBy(u => u.Country.Title).Count();
            this.UniqCities = usersWithCity.GroupBy(u => u.City.Title).Count();

            this.GEO = this.ParseGEO(users.Where(u => u.Country != null));

            this.Platforms = users.Where(u => u.Last_seen != null)
                .Select(u => u.Last_seen)
                .GroupBy(k => k.Platform, i => i)
                .ToDictionary(k => k.Key, v => v.Count());

            if (previousIds != null)
            {
                this.PreviousUserIds = previousIds;
                this.CurrentUserIds = users.Select(u => u.Id);
            }
        }
        
        public void Save(IConfigurationRoot config)
        {
            string format = "N1";
            var sb = new StringBuilder();

            sb.AppendLine("Всего: " + this.Count);
            sb.AppendLine("Ж: " + this.FemalesCount + " (" + this.FemalesPercent.ToString(format) + "%)");
            sb.AppendLine("Самое популярное имя: " + this.FemaleMostName + " (" + this.FemaleMostNameCount + ")");
            sb.AppendLine();

            sb.AppendLine("М: " + this.MalesCount + " (" + this.MalesPercent.ToString(format) + "%)");
            sb.AppendLine("Самое популярное имя: " + this.MaleMostName + " (" + this.MaleMostNameCount + ")");
            sb.AppendLine();

            sb.AppendLine("Указали возраст: " + this.HaveAge + " (" + this.HaveAgePer.ToString(format) + "%)");
            sb.AppendLine("Средний возраст: " + this.AvgAge.ToString(format) + " лет");
            sb.AppendLine();

            sb.AppendLine("Ж: " + this.FemalesWithAge + " (" + this.FemalesWithAgePer.ToString(format) + "%)");
            sb.AppendLine("Ср. Ж: " + this.FemalesAvgAge.ToString(format) + " лет");
            sb.AppendLine("М: " + this.MalesWithAge + " (" + this.MalesWithAgePer.ToString(format) + "%)");
            sb.AppendLine("Ср. М: " + this.MalesAvgAge.ToString(format) + " лет");

            var ageNames = new Dictionary<int, string>()
            {
                { 0, "[0..12)" }, { 1, "[12..18)" }, { 2, "[18..27)" }, { 3, "[27..35)" }, { 4, "[35..50)" }, { 5, "[50..**)" }
            };
            foreach (var key in this.Ages.Keys.OrderBy(i => i))
            {
                sb.AppendLine(ageNames[key] + " → " + this.Ages[key] + " (" +
                    ((float)(this.Ages[key] * 100) / this.HaveAge).ToString(format) + "%)");
            }
            sb.AppendLine();

            sb.AppendLine("Указали страну: " + this.HaveCountry + " (" + this.HaveCountryPer.ToString(format) + "%)");
            sb.AppendLine("Указали город: " + this.HaveCity + " (" + this.HaveCityPer.ToString(format) + "%)");
            sb.AppendLine("Всего стран: " + this.UniqCountries);
            sb.AppendLine("Всего городов: " + this.UniqCities);
            sb.AppendLine();

            var top = 10;
            sb.AppendLine("ТОП " + top);
            foreach (var country in GEO.OrderByDescending(c => c.Value.Values.Sum()).Take(top))
            {
                var people = country.Value.Values.Sum();
                sb.AppendLine(country.Key + " " + people + " (" +
                    ((float)(people * 100) / this.HaveCountry).ToString(format) + "%)");
                foreach (var city in country.Value.OrderByDescending(c => c.Value).Take(top))
                {
                    sb.AppendLine("˖˖˖˖˖˖˖˖˖˖" + city.Key + " " + city.Value + " (" +
                        ((float)(city.Value * 100) / people).ToString(format) + "%)");
                }
            }

            if (this.PreviousUserIds != null)
            {
                // common
                var set = new HashSet<int>(this.PreviousUserIds);
                var common = 0;
                foreach (var id in this.CurrentUserIds)
                {
                    if (set.Contains(id)) common++;
                }

                // left
                var left = this.PreviousUserIds.Count() - common;
                
                // new ones
                var news = this.CurrentUserIds.Count() - common;

                sb.AppendLine();
                sb.AppendLine("По сравнению с прошлым разом");
                sb.AppendLine("Тех же пользователей: " + common);
                sb.AppendLine("Ушло: " + left);
                sb.AppendLine("Новых пришло: " + news);
            }

            sb.AppendLine();
            sb.AppendLine("Платформы");
            var activeUsers = this.Platforms.Sum(i => i.Value);
            foreach (var p in this.Platforms.OrderByDescending(i => i.Value))
            {
                sb.AppendLine(PlatformExtensions.ToFriendlyString(p.Key) + ": " + p.Value + " (" +
                    ((float)(p.Value * 100) / activeUsers).ToString(format) + "%)");
            }

            File.WriteAllText(config["outputFile"], sb.ToString());
        }

        private Dictionary<string, Dictionary<string, int>> ParseGEO(IEnumerable<User> users)
        {
            var geo = new Dictionary<string, Dictionary<string, int>>();
            foreach (var user in users)
            {
                var country = user.Country.Title;
                if (!geo.ContainsKey(country))
                {
                    geo.Add(country, new Dictionary<string, int>());
                }

                if (user.City != null)
                {
                    var city = user.City.Title;
                    if (!string.IsNullOrEmpty(city))
                    {
                        if (geo[country].ContainsKey(city))
                        {
                            geo[country][city]++;
                        }
                        else
                        {
                            geo[country].Add(city, 1);
                        }
                    }
                }
            }

            return geo;
        }

        private int GetAge(DateTime bday)
        {
            var now = DateTime.Today;
            int age = now.Year - bday.Year;
            if (bday > now.AddYears(-age)) age--;

            return age;
        }

        private Dictionary<int, int> ParseDOBs(IEnumerable<User> users)
        {
            var ages = new Dictionary<int, int>();

            var culture = new CultureInfo("ru-RU", true);
            foreach (var user in users)
            {
                var dob = user.Bdate;
                var age = GetAge(DateTime.Parse(dob, culture));
                user.Age = age;
                if (ages.ContainsKey(age))
                {
                    ages[age]++;
                }
                else
                {
                    ages.Add(age, 1);
                }
            }

            var result = new Dictionary<int, int>();
            result.Add(0, 0); // < 12
            result.Add(1, 0); // >= 12 < 18
            result.Add(2, 0); // >= 18 < 27
            result.Add(3, 0); // >= 27 < 35
            result.Add(4, 0); // >= 35 < 50
            result.Add(5, 0); // >= 50

            foreach (var age in ages.Keys.OrderBy(i => i))
            {
                if (age < 12)
                {
                    result[0] += ages[age];
                }
                else if (age >= 12 && age < 18)
                {
                    result[1] += ages[age];
                }
                else if (age >= 18 && age < 27)
                {
                    result[2] += ages[age];
                }
                else if (age >= 27 && age < 35)
                {
                    result[3] += ages[age];
                }
                else if (age >= 35 && age < 50)
                {
                    result[4] += ages[age];
                }
                else if (age >= 50)
                {
                    result[5] += ages[age];
                }
            }

            return result;
        }
    }
}
