﻿namespace VKGroupStats
{
    using System.Collections.Generic;

    public class Response
    {
        public int Count { get; set; }

        public List<User> Items { get; set; }
    }
}
