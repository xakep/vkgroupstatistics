﻿namespace VKGroupStats
{
    public class LastSeen
    {
        public string Time { get; set; }

        public Platform Platform { get; set; }
    }
}
