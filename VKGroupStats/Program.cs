﻿namespace VKGroupStats
{
    using Microsoft.Extensions.Configuration;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;

    class Program
    {
        static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("config.json")
                .Build();

            if (string.IsNullOrEmpty(config["token"]))
            {
                Console.WriteLine("Token is not defined");
                return;
            }

            var web = new HttpClient();
            var input = web.GetStringAsync(string.Format(config["url"], config["groupName"], 0, config["token"]));

            var data = JsonConvert.DeserializeObject<Group>(input.Result);
            if (data.Response == null)
            {
                Console.WriteLine("Token has expired");
                return;
            }

            var users = new List<User>();
            users.AddRange(data.Response.Items);

            var count = data.Response.Count;
            var pages = count / 1000 + 1;
            if (pages > 1)
            {
                for (var i = 1; i < pages; i++)
                {
                    Console.WriteLine("getting page #" + i);
                    input = web.GetStringAsync(string.Format(config["url"], config["groupName"], 1000 * i, config["token"]));
                    data = JsonConvert.DeserializeObject<Group>(input.Result);
                    users.AddRange(data.Response.Items);
                }
            }
            Console.WriteLine("users: " + users.Count);

            var previousGroup = config["previous"];
            IEnumerable<int> oldData = null;
            if (!string.IsNullOrEmpty(previousGroup) && File.Exists(previousGroup))
            {
                var group = JsonConvert.DeserializeObject<Group>(File.ReadAllText(previousGroup));
                if (group != null)
                {
                    oldData = group.Response.Items.Select(u => u.Id);
                }
            }

            var stats = new Stats(users, oldData);
            stats.Save(config);

            var backupDir = config["backupDir"];
            if (!string.IsNullOrEmpty(backupDir) && Directory.Exists(backupDir))
            {
                var fileName = Path.Combine(
                    backupDir, "group_" + DateTime.Now.ToString("dd.MM.yyyy") + ".json");
                File.WriteAllText(fileName, JsonConvert.SerializeObject(
                    new Group() { Response = new Response() { Count = users.Count, Items = users } }, Formatting.Indented));
                Console.WriteLine("Backup created: " + fileName);
            }

            Console.WriteLine("Done");
            Console.Read();
        }
    }
}
