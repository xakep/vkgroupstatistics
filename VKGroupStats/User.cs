﻿namespace VKGroupStats
{
    public class User
    {
        public int Id { get; set; }

        public string First_name { get; set; }

        public string Last_name { get; set; }

        public Sex Sex { get; set; }

        public string Bdate { get; set; }

        public City City { get; set; }

        public Country Country { get; set; }

        public int Age { get; set; }

        public LastSeen Last_seen { get; set; }
    }
}
